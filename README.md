# README #

Shell script for periodically update list of the git repositories in the specific directory.

You can use your commands instead of teams in the program. To use, modify the git instructions in the program.

### How to use ###

Insert the following line in the crontab. Run every 2 min.

"*/2 * * * * cd /path/to/script/ && ./gitupdater"